<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Equipos del centro</h4>
            </div>
            <div class="modal-body">
                    <div id="table" class="table table-responsive">
                        <table id="equiposreparar" class="table table-bordered" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Tipo de equipo</th>
                                <th>Marca</th>
                                <th>Serial</th>
                                <th>Bien nacional</th>

                            </tr>
                            </thead>
                            <tbody>


                            </tbody>
                        </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

    </div>
</div>