<div class="alert alert-info fade in" role="alert">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <p>{{Session::get('message')}}</p>
</div>